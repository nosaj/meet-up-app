function initiateContacts() {
    // specify contact search criteria
    var options = new ContactFindOptions();

    options.filter = "";      // empty search string returns all contacts
    options.multiple = true;  // return multiple results
    filter = ["displayName","phoneNumbers"]; // return contact.displayName field
            
    // find contacts
    navigator.contacts.find(filter, onSuccess, onError, options);
    var availableTags = [];
    function onSuccess(contacts) {
        for (var i = 0; i < contacts.length; i++) {
            availableTags.push(contacts[i].displayName);
        }
        $(function() {
            $("#contact").autocomplete({
                source: availableTags,
                select: function(event, ui) {
                    if (ui.item) {
                        for (var j = 0; j < contacts.length; j++) {
                            if (contacts[j].displayName == ui.item.value) {
                                $('#numberTxt').val(contacts[j].phoneNumbers[0].value);
                            }
                        }
                    }
                }
            });
        });
    };
    function onError(contactError) {
        alert(contactError);
    }
}
