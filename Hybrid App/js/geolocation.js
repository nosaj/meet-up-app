$(document).ready(function() {
    function startGeolocation() {
        setInterval(function() {
            getLocation();
        }, 8000);
    }
    startGeolocation();
    var noGPSError = false;
    function error(error) {
        if (error.code == 3 && noGPSError == false) {
            noGPSError = true;
            navigator.notification.alert(
                'Enable your GPS to continue.', // message
                '', // callback
                'GPS Error', // title
                'Done'                  // buttonName
                );
        }
    }
    var options = { enableHighAccuracy: true, timeout: 3000, maximumAge: 0 };
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition, error, options);
        }
        else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        noGPSError = false;
        var myLat = position.coords.latitude;
        var myLon = position.coords.longitude;
        var addressLat = $('#thereLat').val();
        var addressLon = $('#thereLon').val();
        $('#myLat').val(myLat);
        $('#myLon').val(myLon);
        var smsRange = $('#chooseRange').val();
        var distanceKm = findDistance(myLat, myLon, addressLat, addressLon);
        if (distanceKm <= smsRange && ($('#start')[0].innerHTML) == "Stop App") {
            secondSMSSent = true;
            SendSecondSMS();
        }
        if (distanceKm != false) {
            $('#distanceTo').val(distanceKm + "km's to destination");
        }
    }
});

/*******************/ /* GEOCODER */ /*******************/ 
var geocoder; 
var address;

geocoder = new google.maps.Geocoder();
function getCoord(address) {
    geocoder.geocode({ 'address': address}, function(results, status) {
        var lat = results[0].geometry.location.lat();
        var lon = results[0].geometry.location.lng();
        $('#thereLat').val(lat);
        $('#thereLon').val(lon);
    });
} 

/*********************/
/* Haversine formula */
/*********************/

var earthRadius = 6373;

function findDistance(userLat, userLong, eventLat, eventLong) {
    if (eventLat == "" || eventLong == "") {
        return false;
    }
    var lat1, lon1, lat2, lon2, dlat, dlon, a, c, distanceKm, kmRound;

    // convert coordinates to radians
    lat1 = deg2rad(userLat);
    lon1 = deg2rad(userLong);
    lat2 = deg2rad(eventLat);
    lon2 = deg2rad(eventLong);

    // find the differences between the coordinates
    dlat = lat2 - lat1;
    dlon = lon2 - lon1;

    // calculate the distance with this form
    a = Math.pow(Math.sin(dlat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon / 2), 2);
    c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); // great circle distance in radians
    distanceKm = c * earthRadius;

    // round the results down to the nearest 1/1000
    kmRound = round(distanceKm);
    //display value
    return kmRound;
}

// convert degrees to radians
function deg2rad(deg) {
    rad = deg * Math.PI / 180; // radians = degrees * pi/180
    return rad;
}

// round to the nearest 1/1000
function round(x) {
    return Math.round(x * 1000) / 1000;
}

// Create the search box and link it to the UI element.
var input = (document.getElementById("address"));
var searchBox = new google.maps.places.SearchBox((input));