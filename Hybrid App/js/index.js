
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        navigator.splashscreen.hide();
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

$('#start').click(function() {
    var startStop = ($('#start')[0].innerHTML);
    if (startStop == "Start App") {
        if ($("#address").val() == "" || $("#numberTxt").val() == "" || $("#contact").val() == "") { 
            alert("Fill out a valid contact & address to start the app");
        }
        else {
            startApp();
        }
    }
    else {
        stopApp();
    }
});

function startApp() {
    $('#start').text('Stop App');
    $('#start').toggleClass('stopButton');
    $("#address").prop('disabled', true);
    $("#contact").prop('disabled', true);
    $("#chooseRange").prop('disabled', true);
    $("#extraNotes").prop('disabled', true);
    var theAddress = $("#address").val();
    getCoord(theAddress);
    $("#distanceTo").fadeIn();
    SendFirstSMS();
}
function stopApp() {
    isSecondSmsSent = false;
    $('#start').text('Start App');
    $('#start').toggleClass('stopButton');
    $("#address").prop('disabled', false);
    $("#contact").prop('disabled', false);
    $("#chooseRange").prop('disabled', false);
    $("#extraNotes").prop('disabled', false);
    $("#distanceTo").fadeOut().delay(80).val("Calculating Distance...");
}

function checkInternet() {
    var networkState = navigator.connection.type;
    if (networkState == Connection.NONE) {
        alert("Turn on your internet and GPS then restart the app.");
        navigator.app.exitApp();
    }else{
        return true;
    }
}
