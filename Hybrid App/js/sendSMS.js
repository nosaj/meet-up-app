function SendFirstSMS() {
    var number = $("#numberTxt").val();
    var extraNotes = $("#extraNotes").val();
    var distance = $("#chooseRange").val();
    var message = "Greetings, Just letting you know I am on the way. You'll recieve a sms when I am " + distance + " km/s away. PS: " + extraNotes;
    var intent = ""; //leave empty for sending sms using default intent
    var success = function () {
        smsFirstNotification();
    };
    var error = function (e) {
        alert('Message Failed:' + e);
    };
    sms.send(number, message, intent, success, error);
}

var isSecondSmsSent = false;

function SendSecondSMS() {
    if (isSecondSmsSent == false) {
        var number = $("#numberTxt").val();
        var distance = $("#chooseRange").val();
        var message = "Greetings, Just letting you know I am currently " + distance + " km/s away from you.";
        var intent = ""; //leave empty for sending sms using default intent
        var success = function () {
            smsSecondNotification();
        };
        var error = function (e) {
            alert('Message Failed:' + e);
        };
        sms.send(number, message, intent, success, error);
    }

    isSecondSmsSent = true;
}
function smsFirstNotification() {
    navigator.notification.beep(1);
    navigator.notification.vibrate(1000);
}
function smsSecondNotification() {
    navigator.notification.beep(2);
    navigator.notification.vibrate(3000);
    navigator.notification.alert(
        'Message has been sent that you are on the way', // message
        '', // callback
        'SMS Notification', // title
        'Done'                  // buttonName
        );
}